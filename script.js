var cardsData = [1,2,3,4,5,6,7,8,9];
function initCards(cardsData){
    var cardsContainer = document.getElementById("cards-container");
    cardsContainer.innerHTML = '';
    for(var i=0; i < cardsData.length; i++){
        var cardElement = document.createElement('div');
        cardElement.innerText = cardsData[i];
        cardsContainer.appendChild(cardElement);
    }
}
initCards(cardsData);
function randomShuffle(arr) {
    var shuffItem = arr.length, temp, randomIndex;
    while (0 !== shuffItem) {
        randomIndex = Math.floor(Math.random() * shuffItem);
        shuffItem -- ;
        temp = arr[shuffItem];
        arr[shuffItem] = arr[randomIndex];
        arr[randomIndex] = temp;
    }
    return arr;
}
function shuffleHandler(){
    randomShuffle(cardsData);
    initCards(cardsData);
}
function sortHandler(){
    cardsData.sort(function(a, b) {
        return a - b;
    });
    initCards(cardsData);
}